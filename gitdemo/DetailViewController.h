//
//  DetailViewController.h
//  gitdemo
//
//  Created by zhangkq on 14-6-5.
//  Copyright (c) 2014年 aaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (strong, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end
